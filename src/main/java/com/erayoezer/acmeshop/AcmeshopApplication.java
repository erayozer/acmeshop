package com.erayoezer.acmeshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcmeshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcmeshopApplication.class, args);
	}

}
